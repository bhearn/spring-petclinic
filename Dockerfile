FROM eclipse-temurin:17-jre-jammy 

# This artifact is copied over from the earlier "build" stage
COPY ./target/spring-petclinic-3.3.0-SNAPSHOT.jar /app/spring-petclinic-3.3.0-SNAPSHOT.jar

EXPOSE 8080

ENTRYPOINT java -jar /app/spring-petclinic-3.3.0-SNAPSHOT.jar