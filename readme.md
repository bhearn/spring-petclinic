# Spring PetClinic Sample Application 

A sample Spring-based application

### Tasks

- [x] Use Spring pet-clinic [https://github.com/spring-projects/spring-petclinic](https://github.com/spring-projects/spring-petclinic) as your project source code
- [x] Build a CI/CD pipeline of your choice: `Gitlab CI`
    - [x] Compile the code
    - [x] Run the tests
    - [x] Package the project as a runnable Docker image
- [x] Make sure all dependencies are resolved from JCenter
- [x] Use JFrog Artifactory in your pipeline

### Deliverables

- [x] Gitlab link to the repo including:
    - [x] CI/CD file within that repo
    - [x] Dockerfile within that repo
    - [x] readme.md file explaining the work and how to run the project
- [x] Attached runnable docker image and the command to run it

### Run

**Prerequisites:**
- Docker >= 20.10 installed and running

**Steps:**
1. From this Gitlab repository, navigate to the latest successful pipeline and download the image artifact from the "compile" stage: ie. `https://gitlab.com/bhearn/spring-petclinic/-/jobs/6975882998/artifacts/raw/spring-petclinic.tar`
2. Open a terminal of your choice and change into the directory where the file was downloaded: `cd Downloads`
3. Load the image: `docker load -i spring-petclinic.tar`
4. Run the container: `docker run -p 8080:8080 visitorbla7010670.jfrog.io/docker-trial/spring-petclinic:2fce9388`
5. Open `http://localhost:8080/` in a browser and enjoy the PetClinic App!!!!

### Work

1. Forked the GitHub repo over to my Gitlab account
2. Used JFrog blogs and examples to set up the gitlab-ci.yml boilerplate
3. Followed Youtube tutorials for setting up Artifactory trial version
4. Initially tried building the app with Gradle and was met with a lot of errors and deprecation warnings
5. Eventually decided to switch to building the app with Maven and things began to work more smoothly
6. Learned alot about Gradle, Maven, JCenter, and JFrog
7. Needed to supply a custom settings.xml file to Maven in order to resolve dependencies from JCenter
8. XRay threw errors in the pipeline due to some mysterious policy rule: `No Xray “Fail build in case of a violation” policy rule has been defined on this build`
9. Added "test" stage and all tests passed
10. Utilized the .jar file from "build" stage to copy it into the Dockerfile via Gitlab CI artifact
11. Used docker-in-docker to login to Artifactory registry, build the image, tag the image, push the image to Artifactory, and save the image for easy access
11. Tested the container and documented how to run it

### Resources

- [Gitlab and Artifactory](https://jfrog.com/blog/gitlab-and-artifactory-on-your-mark-get-set-build/)
- [JFrog Gitlab Pipeline Examples](https://github.com/jfrog/project-examples/tree/master/gitlab-examples/gitlab-pipeline)
- [JFrog CLI Authentication Docs](https://docs.jfrog-applications.jfrog.io/jfrog-applications/jfrog-cli/cli-for-jfrog-artifactory/authentication)
- [Maven Quickstart Guide](https://jfrog.com/help/r/get-started-with-the-jfrog-platform/quickstart-guide-maven-and-gradle)
- [Maven Blog](https://jfrog.com/blog/take-full-control-of-your-maven-deployments/)
- [Gradle: Artifactory Plugin](https://plugins.gradle.org/plugin/com.jfrog.artifactory)
- Repository Gradle Client Plugin Resolver - Repository Key: `Bintray -> jcenter`
    - [No more JFrog submissions to JCenter as of March 31st 2021](https://jfrog.com/blog/into-the-sunset-bintray-jcenter-gocenter-and-chartcenter/)
    - [JCenter shutdown as of February 1st, 2022](https://blog.gradle.org/jcenter-shutdown)
- build.gradle `maven = true` was replaced by `ivy { mavenCompatible = true }`
    - [JFrog: Gradle Artifactory Plugin](https://jfrog.com/help/r/jfrog-integrations-documentation/use-the-artifactory-plugin-dsl)
- build.gradle method `artifactory { resolve() }` is no longer supported
- `jf rt mvnc` was deprecated
- [Enable XRay](https://jfrog.com/help/r/xray-how-to-index-and-scan-all-builds-in-xray-in-the-unified-platform/xray-how-to-index-and-scan-all-builds-in-xray-in-the-unified-platform)